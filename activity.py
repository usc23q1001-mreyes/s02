# part 1
name = 'Miguel Angel Reyes'
age = 22
occupation = 'Web Developer'
movie = 'Puss in Boots: The Last Wish'
movie_rating = 100.0

# part 2
print(f'I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for the movie "{movie}" is {movie_rating}%')


# part 3
num1, num2, num3 = 2, 75, 100

# 3.1
print(num1 * num2)

# 3.2
print(num1 < num2)
  
# 3.3
num3 += num2
print(f'num3: {num3}')
